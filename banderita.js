var arduino = require('johnny-five');
var circuito = arduino.Board();
var ledRojo;
var motor;
var termometro;
var opcionesTermometro;

circuito.on('ready', encender);

function encender() {
  var ledRojo = new arduino.Led(13);
  termometro = new arduino.Sensor({
    pin: 'A0'
  });

  termometro.on('data', function data(temperatura) {
    console.log(temperatura);
    if (temperatura >= 150) {
      ledRojo.off();
    } else {
      ledRojo.on();
    }
  });
  ledRojo.on();
}

function leerTemperatura(data) {}
